package softisoft.sms.sender;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.client.utils.URLEncodedUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class BHMobileSMSkoActivity extends Activity {
//	private static final String TAG = "BHMobileSMSkoActivity";
	private ConnectionManager cm;
	private List<NameValuePair> params;
	private SharedPreferences preferences;
	private Button loginButton;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.main);
      
      cm = ((ConnectionManager)this.getApplication());
      
      getWindow().setSoftInputMode(
    		    WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
      
      loadPreferences();
	  
	  loginButton = (Button) findViewById(R.id.loginButton);
	  loginButton.setOnClickListener(new OnClickListener() {
		  public void onClick(View v) {
			  if (login()) {
				  goToSendActivity();
			  } 
		  }
	  });
    }
    
    /**  */
    @Override
    public void onResume(){
    	super.onResume();
    	cm.getThreadSafeClient();
    }
    /** Poziva se prilikom logovanja korisnika na bhmobile.* */
    public boolean login(){
		HttpEntity entity;
		HttpResponse response;
		HttpPost post;
		if (getLoginData()) {
			setParameters();
			try {
				ConnectionManager.httpclient.getCookieStore().clear();
				URI uri = URIUtils.createURI("http", "sso.bhmobile.ba", -1, "sso/login", 
					    URLEncodedUtils.format(params, "UTF-8"), null);
				
				post =  new HttpPost(uri);
			    
				response = cm.getHttpclient().execute(post);
	            entity = response.getEntity();
	                        
	            if (cm.getHttpclient().getCookieStore().getCookies().isEmpty()) {
	                Toast.makeText(getApplicationContext(), "Neuspje�an login.", Toast.LENGTH_LONG).show();
	                entity.consumeContent();
	                return false;
	            }
	            return true;
			} catch (Exception e) {
				showAlert("Gre�ka", "Provjerite da li imate pristup internetu.");
				return false;
			}
		}
		else {
			return false;
		}
	}
    
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// We have only one menu option
		case R.id.arhiva:
			// Launch Preference activity
			Intent intentHistory = new Intent(BHMobileSMSkoActivity.this, MyExpandableList.class);
			startActivity(intentHistory);
			break;
		case R.id.about_app:
			// Launch Preference activity
			Intent intentAboutApp = new Intent(BHMobileSMSkoActivity.this, About.class);
			startActivity(intentAboutApp);
			break;
		case R.id.about_author:
			// Launch Preference activity
			Intent intentAboutAuthor = new Intent(BHMobileSMSkoActivity.this, AboutAuthor.class);
			startActivity(intentAboutAuthor);
			break;
		case R.id.prefs:
			// Launch Preference activity
			Intent intentPreferences = new Intent(BHMobileSMSkoActivity.this, Preferences.class);
			startActivity(intentPreferences);
			break;
		}
		return true;
	}
    
    private void setParameters() {
		params = new ArrayList<NameValuePair>();
	    params.add(new BasicNameValuePair("username", cm.getUsername()));
	    params.add(new BasicNameValuePair("pass", cm.getPassword()));
	    params.add(new BasicNameValuePair("realm", "sso"));
	    params.add(new BasicNameValuePair("application", "portal"));
	    params.add(new BasicNameValuePair("url", "http://www.bhmobile.ba/"));
	    params.add(new BasicNameValuePair("userid", cm.getUsername()));
	    params.add(new BasicNameValuePair("password", cm.getPassword()));
    }
    
    private boolean getLoginData(){
    	EditText usernameEditText;
    	EditText passEditText;
    	
    	try {
    		usernameEditText = (EditText) findViewById(R.id.username);
    		passEditText = (EditText) findViewById(R.id.password);
    		
    		cm.setUsername(usernameEditText.getText().toString());
    		cm.setPassword(passEditText.getText().toString());
    		
    		if( (cm.getUsername().length()==9 || cm.getUsername().length()==10) && cm.getPassword().length()>0 ){
    			String prefix = cm.getUsername().substring(0,3);
    			if (prefix.equals("061") || prefix.equals("062")|| prefix.equals("060")){
    				return true;
    			}
    			else{
    				Toast.makeText(getApplicationContext(),
    						"Broj mora biti u odgovarajucem formatu!", 
    						Toast.LENGTH_LONG).show();
    				return false;
    			}
    		}
    		else{
    			Toast.makeText(getApplicationContext(),
						"Nepravilan format login podataka.", 
						Toast.LENGTH_LONG).show();
    			return false;
    		}
    		
    	    		
		} catch (Exception e) {
			showAlert("Gre�ka", e.getMessage());
			return false;
		}
    }
    
    private void goToSendActivity(){
    	try {
    		Intent intent = new Intent(this.getBaseContext(),SendSMSForm.class);
    		startActivity(intent);
		} catch (Exception e) {
	    	showAlert("Gre�ka", e.getMessage());
		}
    }
    
    private void loadPreferences(){
		preferences = PreferenceManager.getDefaultSharedPreferences(this);
		  
		// Set the values of the UI
		EditText usernameET = (EditText)findViewById(R.id.username);
		usernameET.setText(preferences.getString("username", ""));
		  
		  
		EditText passwordET = (EditText)findViewById(R.id.password);
		passwordET.setText(preferences.getString("password", ""));
		 
		CheckBox chkSave = (CheckBox)findViewById(R.id.chBoxSave);
		chkSave.setChecked(preferences.getBoolean("save_ch_box", false));
    }
    
    
    public void showAlert(String title,String message){
    	AlertDialog alertDialog = new AlertDialog.Builder(this).create();
    	alertDialog.setTitle(title);
    	alertDialog.setMessage(message);
    	alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
    	   public void onClick(DialogInterface dialog, int which) {
    	      // here you can add functions
    	   }
    	});
    	alertDialog.setIcon(R.drawable.icon);
    	alertDialog.show();
    }
    
    @Override
    protected void onPause() 
    {
      super.onPause();
      SharedPreferences.Editor editor = preferences.edit();
      // Put the values from the UI
      CheckBox chBoxSave = (CheckBox)findViewById(R.id.chBoxSave);
      boolean blnSave = chBoxSave.isChecked();
      if (blnSave) {
          EditText txtName = (EditText)findViewById(R.id.username);
          String strName = txtName.getText().toString();
          
          EditText txtEmail = (EditText)findViewById(R.id.password);
          String strEmail = txtEmail.getText().toString();
          editor.putString("username", strName); // value to store
          editor.putString("password", strEmail); // value to store
      }
      editor.putBoolean("save_ch_box", blnSave); // value to store    
      editor.commit();
    }
    
    @Override
    protected void onDestroy(){
    	super.onDestroy();
    	if (isFinishing()) {
    		cm.shutdown();
		}
    }
    
    
}