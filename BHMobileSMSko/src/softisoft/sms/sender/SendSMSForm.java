package softisoft.sms.sender;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SendSMSForm extends Activity {
	private static final int DIALOG_MESSAGE_ID = 100;
	
	private static final int STARTUP = 0;
	private static final int IDLE = 1;
	private static final int SENDING = 2;
	private static final int MESSAGE_SENT = 0;
	private static final int MESSAGE_NOT_SENT= 1;

	
	private static int status = STARTUP;
	private static int messageStatus;
	
	@SuppressWarnings({ "serial", "unused" })
	private static final List<String> prefixList1 = Collections.unmodifiableList(
			new ArrayList<String>(){{add("60");add("61");add("62");add("63");add("65");add("66");}}
			);

	private SharedPreferences preferences;
//	private static final String TAG = "SendSMSForm";
	
	private ConnectionManager cm;
//	private EditText mobitelEditText;
	private EditText contentEditText;
	private TextView numOfSendMessages;
	private TextView remainingCharacters;
	private AutoCompleteTextView mPerson;
	private boolean  smskoArhiva = false;
	private boolean  defaultInbox = false;
	
	private ProgressThread progThread;
    private static ProgressDialog progDialog;
    
	 /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.send_sms_form);
        
        if (status == STARTUP) {
			status = IDLE;
		}
        System.out.println("USAOOOo");
        cm = ((ConnectionManager)this.getApplication());
//        mobitelEditText = (EditText) findViewById(R.id.mobitelEditText);
		contentEditText = (EditText) findViewById(R.id.messageContent);
		
		remainingCharacters = (TextView)findViewById(R.id.preostaloZnakova);
		
		loadPreferences();
		
		contentEditText.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				remainingCharacters.setText(String.valueOf(148 - s.length()));  
			}
		});
		
		cm.openDatabase();
		cm.getDbHelper().clearCounter();
		
		numOfSendMessages = (TextView)findViewById(R.id.broj_poruka);
		numOfSendMessages.setText(" " + cm.getDbHelper().getSendToday(cm.getUsername()));
		
		
        Cursor cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null, null, null, null);
        
        String[] fields = new String[] {
       		 ContactsContract.CommonDataKinds.Phone.NUMBER,
       	     ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
        };
        
       PhonesCursorAdapter myAdapter = 
       	new PhonesCursorAdapter(this, 
       							R.layout.auto_complete_main, 
       							cursor, 
       							fields, 
       							new int[] {R.id.NAME,R.id.NUMBER});
        
        mPerson = (AutoCompleteTextView) findViewById(R.id.person);
//        mPerson.setThreshold(3);
        mPerson.setAdapter(myAdapter);
		
		
        Button sendButton = (Button) findViewById(R.id.sendButton);
        sendButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (getSendingData(true)) {
					showDialog(DIALOG_MESSAGE_ID);
					startSending();
				}
			}
        });
    }
    
    protected void onResume(){
    	super.onResume();
    	numOfSendMessages.setText(" " + cm.getDbHelper().getSendToday(cm.getUsername()));
    }
    
    
    public boolean getSendingData(boolean showWarnings){
    	try {    		
    		cm.setSendingTo(mPerson.getText().toString());
    		if(contentEditText.getText().toString().length()>147)
    			cm.setMessage(contentEditText.getText().toString().substring(0,147));
    		else
    			cm.setMessage(contentEditText.getText().toString());
    		int sendingToLength = cm.getSendingTo().length();
    		if(sendingToLength<9){
    			showWarning("");
    			return false;
    		} 
    		return true;
    		// parsiranje broja na koji se salje
    		
    		//String prefix = "";
    		//String broj = cm.getSendingTo().substring(sendingToLength-6,sendingToLength);
    		
    		
    		/*
    		if(!isNumber(broj)){
    			showWarning("");
    			return false;
    		} 
    		
    		switch (sendingToLength) {
			case 9:
    			prefix = cm.getSendingTo().substring(1,3);
    			if ( prefixList1.contains(prefix)){
    				return true;
    			}else{
    				if (showWarnings) 
    					showWarning("");
    				return false;
    			}
			case 11:
				prefix = cm.getSendingTo().substring(3,5);
    			if (prefixList1.contains(prefix)){
    				return true;
    			}else{
    				if (showWarnings) 
    					showWarning("");
    				return false;
    			}
			case 12:
				prefix = cm.getSendingTo().substring(4,6);
    			if (prefixList1.contains(prefix)){
    				return true;
    			}else{
    				if (showWarnings) 
    					showWarning("");
    				return false;
    			}
				default:
					if (showWarnings) {
						showWarning("");
	    			}
	    			return false;
			}
			*/
		} catch (Exception e) {
			showAlert("Gre�ka", e.getMessage());
			return false;
		}
    }
    
    @SuppressWarnings("unused")
	private boolean isNumber(String in){
    	 try {
             Integer.parseInt(in);
         } catch (NumberFormatException ex) {
             return false;
         }
         return true;
    }
    
    private void showWarning(String format){
    	String message;
    	if(format.length()>0)
    		message = "Broj mora biti u formatu \n" + format;
    	else
    		message = "Nepravilan format podataka za slanje.";
    	Toast.makeText(getApplicationContext(),message, Toast.LENGTH_LONG).show();
    }
    
    public void sendSMS(){
		HttpPost postSMS;
		HttpResponse responseSMS;
		HttpEntity entitySMS=null;
		try {
			List<NameValuePair> params = new ArrayList<NameValuePair>();
		    params.add(new BasicNameValuePair("idc", "1023422"));
		    params.add(new BasicNameValuePair("subtype", "web2sms_arhiva"));
		    params.add(new BasicNameValuePair("primatelj", cm.getSendingTo()));
		    params.add(new BasicNameValuePair("tekst_poruke", cm.getMessage()));
		    params.add(new BasicNameValuePair("action", "send_sms_submit"));
			
		    URI uri = URIUtils.createURI("http", "www.bhmobile.ba", -1, "portal/show", 
				    URLEncodedUtils.format(params, "UTF-8"), null);
		    postSMS = new HttpPost(uri);
		    
		    responseSMS = cm.getHttpclient().execute(postSMS);
		    entitySMS = responseSMS.getEntity();
		    
            entitySMS.consumeContent(); 
            messageStatus = MESSAGE_SENT;
            
		} catch (Exception e) {
			messageStatus = MESSAGE_NOT_SENT;
			showAlert("Gre�ka prilikom slanja", e.getMessage());
		}
	}
    
    private void clearFields(){
    	try {
    		cm.setSendingTo("");
    		cm.setMessage("");
    		mPerson.setText("");
    		contentEditText.setText("");
    	}
    	catch(Exception e){
    		e.printStackTrace();
    	}
    }
    
    private void addMessageToSent() {
    
    	Intent addMessageIntent = new Intent(this, SentSmsLogger.class);
    	addMessageIntent.putExtra("telNumber",cm.getSendingTo() );
    	addMessageIntent.putExtra("messageBody",cm.getMessage() );
    	startService(addMessageIntent);
  
    }
    
    private void loadPreferences(){
		preferences = PreferenceManager.getDefaultSharedPreferences(this);
		  
		// Pokupi prefernece
		smskoArhiva = preferences.getBoolean("smskoArhiva", false);
		defaultInbox = preferences.getBoolean("defaultInbox", false);
    }
    
    
    public void showAlert(String title,String message){
    	AlertDialog alertDialog = new AlertDialog.Builder(this).create();
    	alertDialog.setTitle(title);
    	alertDialog.setMessage(message);
    	alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
    	   public void onClick(DialogInterface dialog, int which) {
    	      // here you can add functions
    	   }
    	});
    	alertDialog.setIcon(R.drawable.icon);
    	alertDialog.show();
    }

    public void showProgress(){
    	if (getSendingData(false) && status==IDLE) {
        	progDialog = new ProgressDialog(this);
            progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDialog.setMessage("Slanje poruke u toku...");
            progDialog.show();
            progThread = new ProgressThread(handler);
            progThread.start(); 
        }
    }
    
    public void restoreProgress(){
    	if (status == SENDING) {
    		progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDialog.setMessage("Slanje poruke u toku...");
            progDialog.show();
		}
    }
    
    public void startSending(){
    	if (getSendingData(false) && status==IDLE) {
			progThread = new ProgressThread(handler);
			progThread.start();
		}
    }
    
    @Override
    protected Dialog onCreateDialog(int id) {
    	switch(id) {
        case DIALOG_MESSAGE_ID:                      // Spinner
	        if (getSendingData(false)) {
	        	progDialog = new ProgressDialog(this);
	            progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	            progDialog.setMessage("Slanje poruke u toku...");
            }
	        else{
	        	progDialog = null;
	        }
            break;        
        default:
        	progDialog = null;
        }
    	return progDialog;
    }
    
    final Handler handler = new Handler() {
        public void handleMessage(Message msg) {
        	try {
				int inserted = msg.getData().getInt("send");
				if (inserted == 1){
	                Toast.makeText(SendSMSForm.this,
	            			"Poruka poslana na: " + cm.getSendingTo(), 
	            			Toast.LENGTH_LONG).show();
	                
	                if (smskoArhiva) {
	                	cm.getDbHelper().insertMessage(cm.getUsername(), cm.getSendingTo(), cm.getMessage());
					}
	                
	                if (defaultInbox) {
	                	addMessageToSent();
					}
	                
	                cm.getDbHelper().insertMessage(cm.getUsername());
	                
	                numOfSendMessages.setText(" " + cm.getDbHelper().getSendToday(cm.getUsername()));
	                clearFields();
				}else{
					Toast.makeText(SendSMSForm.this,
	            			"Slanje poruke nije uspjelo.", 
	            			Toast.LENGTH_LONG).show();
				}
				 status = IDLE;
				 progDialog.dismiss();
				 removeDialog(DIALOG_MESSAGE_ID);
			} catch (Exception e) {
				showAlert("Greska", e.getMessage());
			}
        }
    };
    
    @Override
    protected void onDestroy(){
    	super.onDestroy();
		if(isFinishing()){
			cm.clearData();
			cm.closeDatabase();
		}
    }
    
    private class ProgressThread extends Thread {	
        Handler mHandler;
        ProgressThread(Handler h) {
            mHandler = h;
        }
        @Override
        public void run() {
            try {
            	if (status == IDLE) {
                	status = SENDING;
                	sendSMS();
                	Message msg = mHandler.obtainMessage();
					Bundle b = new Bundle();
					if (messageStatus==MESSAGE_SENT) {
						b.putInt("send", 1);
					}
					else{
						b.putInt("send", 0);
					}
					msg.setData(b);
					mHandler.sendMessage(msg); 	
				}
            } catch (Exception e) {
            	status = IDLE;
            	Message msg = mHandler.obtainMessage();
				Bundle b = new Bundle();
				b.putInt("send", 0);
				msg.setData(b);
				mHandler.sendMessage(msg);
            	clearFields();
            }
            
        }
    }
}
