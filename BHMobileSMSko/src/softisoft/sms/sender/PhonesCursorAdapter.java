package softisoft.sms.sender;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.widget.Filterable;
import android.widget.SimpleCursorAdapter;

public class PhonesCursorAdapter extends SimpleCursorAdapter  implements Filterable{
	public static final int HOME = 1;
	public static final int MOBILE = 2;
	public static final int WORK = 3;
	
	private ContentResolver mContent;
    long mPeopleId;
    String mPeopleName;
    boolean klik = false;
    
	public PhonesCursorAdapter(Context context, int layout, Cursor c,
			String[] from, int[] to) {
		super(context, layout, c, from, to);
		mContent = context.getContentResolver();
		
	}
    @Override
    public String convertToString(Cursor cursor) {
    	return cursor.getString(1);
    }
    
 // after 2.0
    private static final String[] PEOPLE_PROJECTION = new String[] {
    	ContactsContract.CommonDataKinds.Phone._ID,
    	ContactsContract.CommonDataKinds.Phone.NUMBER,
    	ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
    	ContactsContract.CommonDataKinds.Phone.TYPE

    };
	
	@Override
    public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
        if (getFilterQueryProvider() != null) {
            return getFilterQueryProvider().runQuery(constraint);
        }

        StringBuilder buffer = null;
        String[] args = null;
        if (constraint != null) {
            buffer = new StringBuilder();
            buffer.append(ContactsContract.Contacts.DISPLAY_NAME); //after 2.0
            buffer.append(" like ? ");
            args = new String[] {"%" + constraint.toString() + "%"};
        }
        
        return mContent.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, PEOPLE_PROJECTION,
                buffer == null ? null : buffer.toString(), 
                args,
                null);
    }

}
