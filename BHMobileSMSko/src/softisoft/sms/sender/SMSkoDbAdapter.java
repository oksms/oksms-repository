package softisoft.sms.sender;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class SMSkoDbAdapter {

	// Database fields
	public static final String KEY_ROWID = "_id";
	public static final String KEY_FROM = "sender";
	public static final String KEY_TO = "sending_to";
	public static final String KEY_MESSAGE = "message";
	public static final String KEY_DATE = "send_date";
	private Context context;
	private SQLiteDatabase database;
	private SMSkoDatabaseHelper dbHelper;

	public SMSkoDbAdapter(Context context) {
		this.context = context;
	}

	public SMSkoDbAdapter open() throws SQLException {
		try {
			dbHelper = new SMSkoDatabaseHelper(context);
			database = dbHelper.getWritableDatabase();
			return this;
		} catch (Exception e) {
			return null;
		}
		
	}
	
	public boolean isOpen(){
		return database!=null;
	}
	
	public void close() {
		dbHelper.close();
	}
	
	public long insertMessage(String from, String to, String message) {
		ContentValues initialValues = createContentValues(from, to, message);
		return database.insert(SMSkoDatabaseHelper.TABLE_NAME_1, null, initialValues);
	}
	
	public long insertMessage(String from) {
		ContentValues values = new ContentValues();
		values.put(KEY_FROM, from);
		return database.insert(SMSkoDatabaseHelper.TABLE_NAME_2, null,values);
	}
	
	
	public int getSendToday(String from){
		int messagesSend = 0;
		try {
			Cursor mCursor = database.query(true, SMSkoDatabaseHelper.TABLE_NAME_2, new String[] {
					KEY_ROWID },
					KEY_DATE + "=current_date and " +
					KEY_FROM + "='" + from + "'"
					, null, null, null, null, null);
			messagesSend = mCursor.getCount();
			mCursor.close();
			return  messagesSend;
		} catch (Exception e) {
			return 0;
		}
		
	}
	
	
	public Cursor getMessagesGroupedBySender(){
		
		//database.query(distinct, table, 
		//columns, selection, selectionArgs, groupBy, having, orderBy, limit)
		
		String[] columns = {KEY_ROWID,KEY_FROM,"count(*)"};
		Cursor c = database.query(true, SMSkoDatabaseHelper.TABLE_NAME_1, 
				columns , null, null, KEY_FROM, null, null, null);
		return c;
	}
	
	public Cursor getMessagesSendOn(String from,String date){
		try {
			String selection="";
			if(date.equals("Danas")){
				selection = KEY_DATE + "=current_date and " +
				KEY_FROM + "='" + from + "'";
			}
			else {
				selection = KEY_DATE + "='" + date  + "' and " +
				KEY_FROM + "='" + from + "'";	
			}
			Cursor mCursor = database.query(true, SMSkoDatabaseHelper.TABLE_NAME_1, new String[] {
					KEY_ROWID,KEY_TO, KEY_MESSAGE },
					selection, null, null, null, null, null);
			return  mCursor;
		} catch (Exception e) {
			return null;
		}
		
	}
	
	public Cursor getMessagesGroupedByDate(){
		try {
			String[] columns2 = {KEY_ROWID," CASE " +  KEY_DATE  +
					" WHEN current_date then 'Danas' " +
					" WHEN current_date - 1 THEN 'Jucer'" +
					" ELSE " + KEY_DATE  +
					" END As Date","count(*)"};		
			Cursor c = database.query(true, SMSkoDatabaseHelper.TABLE_NAME_1, 
					columns2 , null, null, "Date", null, null, null);
			return c;
		} catch (Exception e) {
			return null;
		}
	}
	
	public ArrayList<String> getDatesWithNames() {
		ArrayList<String> dates = new ArrayList<String>();
		try {
			String[] columns = {KEY_DATE," CASE " +  KEY_DATE  +
					" WHEN current_date then 'Danas' " +
					" ELSE " + KEY_DATE  +
					" END as datum" };
			Cursor c = database.query(true, SMSkoDatabaseHelper.TABLE_NAME_1, 
					columns , null, null, null, null, KEY_DATE + " desc", null);
			
			if (c.getCount() > 0) {
		     while (c.moveToNext()) {
		         String date = c.getString(c.getColumnIndex("datum"));
		         dates.add(date);
		     }
		    }
			c.close();
			return dates;
		} catch (Exception e) {
			return null;
		}
	}
	
	public ArrayList<String> getDates() {
		ArrayList<String> dates = new ArrayList<String>();
		try {
			String[] columns = {KEY_DATE};		
			Cursor c = database.query(true, SMSkoDatabaseHelper.TABLE_NAME_1, 
					columns , null, null, null, null, KEY_DATE + " desc", null);
			
			if (c.getCount() > 0) {
		     while (c.moveToNext()) {
		         String date = c.getString(c.getColumnIndex(KEY_DATE));
		         dates.add(date);
		     }
		    }
			c.close();
			return dates;
		} catch (Exception e) {
			return null;
		}
		
	}

	public ArrayList<ArrayList<String>> getMessagesOnDate(String date) {
		ArrayList<ArrayList<String>> sendMessages = new ArrayList<ArrayList<String>>();
		try {
			String selection = KEY_DATE + " = '" + date + "'";
			String[] columns = {KEY_FROM,"count(*) as message_count"};
			Cursor c = database.query(true, SMSkoDatabaseHelper.TABLE_NAME_1, 
					columns , selection, null, KEY_FROM , null, null, null);
			if (c.getCount() > 0) {
				int i = 0;
			     while (c.moveToNext()) {
			         String sender = c.getString(c.getColumnIndex(KEY_FROM));
			         int numOfMessages = c.getInt(c.getColumnIndex("message_count"));
			         sendMessages.add(new ArrayList<String>());
			         sendMessages.get(i).add(sender + "      " + numOfMessages);
			         i++;
			     }
			}
			c.close();
			return sendMessages;
		} catch (Exception e) {
			return null;
		}
	}
	
	public int getMessageCount(){
		int count=0;
		try {
			String[] columns = {"count(*) as message_count"};
			Cursor c = database.query(true, SMSkoDatabaseHelper.TABLE_NAME_1, 
					columns , null, null, null , null, null, null);
			if (c.getCount() > 0) {
				c.moveToFirst();
			    count = c.getInt(c.getColumnIndex("message_count"));
			}
			c.close();
			return count;
		} catch (Exception e) {
			return 0;
		}
	}
	
	public void clearCounter() {
		try {
			database.delete(SMSkoDatabaseHelper.TABLE_NAME_2, KEY_DATE +  "!=current_date", null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void clearHistory() {
		try {
			database.delete(SMSkoDatabaseHelper.TABLE_NAME_1, null, null);
		} catch (Exception e) {
		}
		
	}
	
	private ContentValues createContentValues(String from, String to,
			String message) {
		ContentValues values = new ContentValues();
		values.put(KEY_FROM, from);
		values.put(KEY_TO, to);
		values.put(KEY_MESSAGE, message);
		return values;
	}

}
