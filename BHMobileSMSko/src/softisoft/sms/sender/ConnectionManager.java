package softisoft.sms.sender;

import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.HttpParams;

import android.app.Application;
import android.database.SQLException;

public class ConnectionManager extends Application{
	public static DefaultHttpClient httpclient = null;
	private SMSkoDbAdapter dbHelper;
	private String username;
	private String password;
	private String sendingTo;
	private String message;
	
	@Override
	public void onCreate() {
		super.onCreate();
		getThreadSafeClient();
	}
	
	public void getThreadSafeClient()  {

		if (httpclient != null)
            return;
         
		httpclient = new DefaultHttpClient();
        
        ClientConnectionManager mgr = httpclient.getConnectionManager();
        
        HttpParams params = httpclient.getParams();
        httpclient = new DefaultHttpClient(
        new ThreadSafeClientConnManager(params,
            mgr.getSchemeRegistry()), params);
        System.out.println("getThreadSafeClient");
  
	}
	
	public void openDatabase(){
		try {
			dbHelper = new SMSkoDbAdapter(this);
			dbHelper.open();
		} catch (SQLException e) {
		}
	}
	public void closeDatabase(){
		if (dbHelper != null) {
			dbHelper.close();
		}
	}
	
	public void shutdown(){
		try {
			username = "";
			password = "";
			sendingTo = "";
			message = "";
			httpclient.getCookieStore().clear();
			httpclient.getConnectionManager().shutdown();
			httpclient = null;
			closeDatabase();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void clearData() {
		try {
			sendingTo = "";
			message = "";
			username = "";
			password = "";
			httpclient.getCookieStore().clear();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public SMSkoDbAdapter getDbHelper() {
		return dbHelper;
	}

	public DefaultHttpClient getHttpclient() {
		return httpclient;
	}

	public void setHttpclient(DefaultHttpClient httpclient) {
		ConnectionManager.httpclient = httpclient;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSendingTo() {
		return sendingTo;
	}

	public void setSendingTo(String sendingTo) {
		this.sendingTo = sendingTo;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
