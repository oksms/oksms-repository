package softisoft.sms.sender;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SMSkoDatabaseHelper extends SQLiteOpenHelper {
	private static final String DATABASE_NAME = "applicationdata";
	public static final String TABLE_NAME_1 = "sms_history";
	public static final String TABLE_NAME_2 = "sms_counter";

	private static final int DATABASE_VERSION = 3;
	
	// Database creation sql statement
	private static final String DATABASE_CREATE_TABLE_1 = 
			"create table "+ TABLE_NAME_1 +"(" +
			"_id integer primary key autoincrement, "+
			"sender text not null, " +
			"sending_to text not null, " +
			"message text not null," +
			"send_date date default CURRENT_DATE);";
	
	private static final String DATABASE_CREATE_TABLE_2 =
			"create table "+ TABLE_NAME_2 + "(" + 
			"_id integer primary key autoincrement, "+
			"sender text not null, " +
			"send_date date default CURRENT_DATE);"
			;

	public SMSkoDatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// Method is called during creation of the database
	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE_TABLE_1);
		database.execSQL(DATABASE_CREATE_TABLE_2);
	}

	// Method is called during an upgrade of the database, e.g. if you increase
	// the database version
	@Override
	public void onUpgrade(SQLiteDatabase database, int oldVersion,
			int newVersion) {
		database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_1 + ";");
		database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_2 + ";");
		onCreate(database);
	}

}
