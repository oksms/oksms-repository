package softisoft.sms.sender;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

public class MyExpandableList extends Activity {

	private ArrayList<String> groups;
	private ArrayList<ArrayList<ArrayList<String>>> children;
	private TextView messagesTotal;
	private SMSkoDbAdapter dbHelper;
	private ExpandableListView l;
	private myExpandableAdapter adapter;
 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        dbHelper = new SMSkoDbAdapter(this);
        
        setContentView(R.layout.expandable_list);
 
        l = (ExpandableListView) findViewById(R.id.ExpandableListView01);
        
        messagesTotal = (TextView) findViewById(R.id.poslanih_poruka);
        
        loadData();
 
        adapter = new myExpandableAdapter(this, groups, children);
		l.setAdapter(adapter);
    }
    
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_history, menu);
		return true;
	}
    
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.clear_history:
			new AlertDialog.Builder(this)
		    .setTitle("Brisanje historije")
		    .setMessage("Da li ste sigurni da zelite obrisati arhivu?")
		    .setPositiveButton("Da", new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) { 
		        	dbHelper.open();
					dbHelper.clearHistory();
					dbHelper.close();
					reload();
		        }
		     })
		    .setNegativeButton("Ne", new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) { 
		            // do nothing
		        }
		     })
		     .show();
			
			break;
		}
		return true;
	}
	
	 public void reload() {
		    Intent intent = getIntent();		    
		    finish();
		    startActivity(intent);
		}
    
    private void loadData(){
    	try {
			children = new ArrayList<ArrayList<ArrayList<String>>>();
			dbHelper.open();
			groups = dbHelper.getDatesWithNames();
			ArrayList<String> tempGr = dbHelper.getDates();
			for (int i = 0; i < tempGr.size(); i++) {
				ArrayList<ArrayList<String>> temp = dbHelper
						.getMessagesOnDate(tempGr.get(i));
				children.add(temp);
			}
			messagesTotal.setText(" " + dbHelper.getMessageCount());
			dbHelper.close();
		} catch (Exception e) {
			Log.i("EXL",e.getMessage());
		}
    }
    
    public class myExpandableAdapter extends BaseExpandableListAdapter {
 
    	private ArrayList<String> groups;
 
        private ArrayList<ArrayList<ArrayList<String>>> children;
 
    	private Context context;
 
    	public myExpandableAdapter(Context context, ArrayList<String> groups, ArrayList<ArrayList<ArrayList<String>>> children) {
            this.context = context;
            this.groups = groups;
            this.children = children;
        }
 
    	public void showHistoryDetails(String groupPosition, String childPosition ){
    		Intent intentHistory = new Intent(context, HistoryDetailsList.class);
    		intentHistory.putExtra("date", groupPosition);
    		intentHistory.putExtra("sender", childPosition.substring(0, 9));
			startActivity(intentHistory);
    	}
 
    	@Override
        public boolean areAllItemsEnabled()
        {
            return true;
        }
 
 
        @Override
        public ArrayList<String> getChild(int groupPosition, int childPosition) {
            return children.get(groupPosition).get(childPosition);
        }
 
        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }
 
 
        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild,View convertView, ViewGroup parent) {
 
        	String child = (String) ((ArrayList<String>)getChild(groupPosition, childPosition)).get(0);
 
            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.expandable_list_child, null);
            }
 
            TextView childtxt = (TextView) convertView.findViewById(R.id.TextViewChild01);
 
            childtxt.setText(child);
 
            final String grpos = groups.get(groupPosition);
            final String chpos = children.get(groupPosition).get(childPosition).get(0);
            childtxt.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					showHistoryDetails(grpos,chpos);
				}
			});
            
            return convertView;
        }
 
        @Override
        public int getChildrenCount(int groupPosition) {
            return children.get(groupPosition).size();
        }
 
        @Override
        public String getGroup(int groupPosition) {
            return groups.get(groupPosition);
        }
 
        @Override
        public int getGroupCount() {
            return groups.size();
        }
 
        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }
 
        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
 
        	String group = (String) getGroup(groupPosition);
 
        	if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.expandable_list_group, null);
            }
 
            TextView grouptxt = (TextView) convertView.findViewById(R.id.TextViewGroup);
 
            grouptxt.setText(group);
 
            return convertView;
        }
 
        @Override
        public boolean hasStableIds() {
            return true;
        }
 
        @Override
        public boolean isChildSelectable(int arg0, int arg1) {
            return true;
        }
    }
    
}
