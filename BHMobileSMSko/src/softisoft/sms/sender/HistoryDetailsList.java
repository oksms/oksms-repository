package softisoft.sms.sender;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class HistoryDetailsList extends Activity {
//	private static final String TAG = "HistoryDetailsList";
	private SMSkoDbAdapter dbHelper;
	private Cursor cursorMessagesOnDate;
	private String date="";
	private String sender="";
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history_details_list);
        
        try {
			dbHelper = new SMSkoDbAdapter(this);
			dbHelper.open();
			
			Bundle extras = getIntent().getExtras();
			if(extras !=null) {
				date = extras.getString("date");
				sender = extras.getString("sender");
			}
			
			// Setup the list view
	        final ListView messagesListView = (ListView) findViewById(R.id.list);
			
			String[] displayFields = new String[] {
					SMSkoDbAdapter.KEY_TO,
					SMSkoDbAdapter.KEY_MESSAGE};
			int[] displayViews = new int[] { R.id.to, R.id.message };
			
			cursorMessagesOnDate = dbHelper.getMessagesSendOn(sender, date);
			SimpleCursorAdapter adapter =  new SimpleCursorAdapter(this, 
			        R.layout.history_details_list_item, cursorMessagesOnDate, 
			        displayFields, displayViews);
			messagesListView.setAdapter(adapter);
		} catch (Exception e) {
		}        
    }
    
    protected void onDestroy(){
    	super.onDestroy();
    	cursorMessagesOnDate.close();
    	dbHelper.close();
    }
}